import React from 'react';
import Loadable from 'react-loadable';

const Loading = (props) => {
  if (props.error) {
    return <div>Error! <button onClick={ props.retry }>Retry</button></div>;
  }
  return <div>Loading...</div>;
}

const AboutComponent = Loadable({
	loader: () => import('./components/Pages/About.js'),
	loading: () => <Loading />,
})

const UserComponent = Loadable({
	loader: () => import('./components/Users/User.js'),
	loading: () => <Loading />,
  render(loaded, props, ownProp) {
    let Component = loaded.default;
    return <Component {...props} {...ownProp}/>;
  }
})

export {
  AboutComponent,
  UserComponent
}