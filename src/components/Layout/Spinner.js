import React, { Fragment } from 'react';
import spinner from '../../img/spinner.gif';

const Spinner = () => <Fragment>
    <img src={spinner} alt="Loading.." style={{ margin: 'auto', width: '200px', height: '200px', display: 'block'}} />
  </Fragment>

export default Spinner;
