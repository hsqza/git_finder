import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Search extends Component {
  static propTypes = {
    searchUsers: PropTypes.func.isRequired,
    clearUsers: PropTypes.func.isRequired,
    showClear: PropTypes.bool.isRequired,
    setAlert: PropTypes.func.isRequired
  }

  state = {
    text: ''
  }

  onChange = e => this.setState({ [e.target.name]: e.target.value })

  onSubmit = e => {
    e.preventDefault();
    const { text } = this.state;
    const { searchUsers, setAlert } = this.props;

    if (text === '') {
      return setAlert('Please enter something', 'light');
    }

    searchUsers(text);
    this.setState({ text: '' });
  }

  render() {
    const { text } = this.state;
    const { clearUsers, showClear } = this.props;
    
    return (
      <div>
        <form className='form' onSubmit={this.onSubmit}>
          <input type='text' name='text' placeholder='Search Users...' onChange={this.onChange} value={text}/>
          <input type='submit' value='Search' className='btn btn-dark btn-block' />
        </form>
        { showClear && <button className="btn btn-light btn-block" onClick={clearUsers}>clear</button> }
      </div>
    )
  }
}

export default Search
