import React, { Fragment, Component } from 'react';
import Spinner from '../Layout/Spinner';
import Repos from '../Repos/Repos';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

export class User extends Component {
  static propTypes = {
    getUser: PropTypes.func.isRequired,
    repos: PropTypes.array.isRequired,
    Spinner: PropTypes.bool,
    user: PropTypes.object.isRequired,
    getUserRepo: PropTypes.func.isRequired,
  }

  componentDidMount() {
    const { getUser, getUserRepo, match } = this.props;

    getUser(match.params.login);
    getUserRepo(match.params.login);
  }
  

  render() {
    const { loading, user, repos } = this.props;
    const {
      login,
      id,
      node_id,
      avatar_url,
      gravatar_id,
      url,
      html_url,
      followers_url,
      following_url,
      gists_url,
      starred_url,
      subscriptions_url,
      organizations_url,
      repos_url,
      events_url,
      received_events_url,
      type,
      site_admin,
      name,
      company,
      blog,
      location,
      email,
      hireable,
      bio,
      public_repos,
      public_gists,
      followers,
      following,
      created_at,
      updated_at
    } = user;

    const lists = [
      { label: 'Username', value: login },
      { label: 'Company', value: company },
      { label: 'Website', value: blog }
    ]

    return (
      <Fragment>
        <Link to='/' className='btn btn-light'>
          Back to Search
        </Link>

        Hireable: {''}
        {hireable ? (
          <i className='fas fa-check text-success' /> 
        ) : ( 
          <i className='fas fa-times-circle text-danger' />
        )}

        <div className="card grid-2">
          <div className="all-center">
            <img src={avatar_url} className='round-img' alt="" style={{ width: '150px' }} />
            <h1>{name}</h1>
            <p>Location: {location}</p>
          </div>
          <div>
            {bio && (
              <Fragment>
                <h3>Bio</h3>
                {bio}
              </Fragment>
            )}
            <a href={html_url} className='btn btn-dark my-1'>Visit Github Profile</a>
            <ul>
              {lists.map((item, i) => (
                <li key={i}>
                  {item.value && <Fragment><strong>{item.label}: </strong> {item.value}</Fragment>}
                </li>
              ))}
            </ul>
          </div>
        </div>

        <div className="card text-center">
          <div className="badge badge-primary">Followers: {followers}</div>
          <div className="badge badge-success">Following: {following}</div>
          <div className="badge badge-info">Public Repos: {public_repos}</div>
          <div className="badge badge-dark">Public Gists: {public_gists}</div>
        </div>

        <Repos repos={repos} />

      </Fragment>
    )
  }
}

export default User
