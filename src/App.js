import React, { Component, Fragment } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import Navbar from './components/Layout/Navbar';
import Users from './components/Users/Users';
import Search from './components/Users/Search';
import Alert from './components/Layout/Alert';
import { AboutComponent, UserComponent } from './routes';

import ApiServices from './api-services';
import './App.css';

class App extends Component {
  state = {
    users: [],
    repos: [],
    user: { name: 'Hubi' },
    loading: false,
    alert: null
  }

  searchUsers = async text => {
    this.setState({ loading: true });

    const res = await ApiServices.searchUsers(text);

    this.setState({ users: res.data.items, loading: false });
  }

  getUser = async (username) => {
    const res = await ApiServices.getUser(username);
    this.setState({ user: res.data, loading: false });
  }

  getUserRepo = async (username) => {
    const res = await ApiServices.getUserRepo(username);
    this.setState({ repos: res.data, loading: false });
  }

  clearUsers = () => {
    this.setState({ users: [], loading: false });
  }

  setAlert = (msg, type) => {
    this.setState({ alert: { msg, type }  })
  
    setTimeout(() => this.setState({ alert: null }), 3000);
  }
  
  render() {
    const {
      users,
      loading,
      alert,
      user,
      repos
    } = this.state;

    return (
      <Router>     
        <div className="App">
          <Navbar />
          <div className="container">
            <Alert alert={alert} />

            <Switch>
              <Route
                exact
                path='/'
                component={props => (
                  <Fragment>
                    <Search
                      searchUsers={this.searchUsers}
                      clearUsers={this.clearUsers}
                      showClear={users.length > 0 && true}
                      setAlert={this.setAlert}
                    />
                    <Users users={users} loading={loading}/>
                  </Fragment>
                )}
              />
              <Route exact path='/about' component={AboutComponent} />
              <Route exact path='/user/:login' render={(props) => 
                <UserComponent
                  user={user}
                  repos={repos}
                  getUser={this.getUser}
                  getUserRepo={this.getUserRepo}
                  loading={loading}
                  {...props}
                /> 
              }/>
            </Switch>
          </div>
        </div>
      </Router>
    );
  }
}

export default App;
