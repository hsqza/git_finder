import axios from 'axios';

axios.defaults.baseURL = process.env.REACT_APP_BASE_URL;

axios.interceptors.request.use(config => {
  config.params = {
    client_id: process.env.REACT_APP_BITBUCEKT_CLIENT_ID,
    client_secret: process.env.REACT_APP_BITBUCEKT_CLIENT_SECRET
  }
  return config;
}, err => {
  return Promise.reject(err);
});

export default axios;
